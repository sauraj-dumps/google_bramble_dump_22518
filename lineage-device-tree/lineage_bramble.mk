#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from bramble device
$(call inherit-product, device/google/bramble/device.mk)

PRODUCT_DEVICE := bramble
PRODUCT_NAME := lineage_bramble
PRODUCT_BRAND := google
PRODUCT_MODEL := Pixel 4a (5G)
PRODUCT_MANUFACTURER := google

PRODUCT_GMS_CLIENTID_BASE := android-google

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="bramble_beta-user UpsideDownCake UPB1.230309.014 9890577 release-keys"

BUILD_FINGERPRINT := google/bramble_beta/bramble:UpsideDownCake/UPB1.230309.014/9890577:user/release-keys
