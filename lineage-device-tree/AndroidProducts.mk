#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_bramble.mk

COMMON_LUNCH_CHOICES := \
    lineage_bramble-user \
    lineage_bramble-userdebug \
    lineage_bramble-eng
